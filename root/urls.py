from django.urls import path
from . import views

from .views import (
    IndexView,
    FrontendView,
    BackendView,
    DogListView,
    dogAddView,
    OwnerListView,
    ownerAddView
)


urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('frontend/', FrontendView.as_view(), name="frontend"),
    path('backend/', BackendView.as_view(), name="backend"),
    path('dog/list', DogListView.as_view(), name="doglist"),
    path('dog/add', views.dogAddView, name="dogadd"),
    path('owner/list', OwnerListView.as_view(), name="ownerlist"),
    path('owner/add', views.ownerAddView, name="owneradd"),
    
]
