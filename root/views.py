from django.shortcuts import render, redirect
from django.views.generic import View
from .models import DogModel, OwnerModel
from .forms import DogModelForm, OwnerModelForm

class IndexView(View):
    template_name = "index.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class FrontendView(View):
    template_name = "frontend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class BackendView(View):
    template_name = "backend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


#Dog Views

class DogListView(View):
    template_name = "./dog/list.html"

    def get(self, request):
        dogs = DogModel.objects.all()
        return render(request, self.template_name, {'dogs': dogs})


def dogAddView(request):
    if request.method == "POST":
        form = DogModelForm(request.POST)
        if form.is_valid():
            dog = form.save(commit=False)
            dog.name = request.POST.get('name')
            dog.age = request.POST.get('age')
            dog.breed = request.POST.get('breed')
            dog.owner = request.POST.get('owner')
            dog.save()
            return redirect('/dog/add')
                
    else:
        form = DogModelForm()
    return render(request, "./dog/add.html" , {'form': form})


#Owner Views
class OwnerListView(View):
    template_name = "./owner/list.html"

    def get(self, request):
        print("AQUIIIII 1")
        owners = OwnerModel.objects.all()
        return render(request, self.template_name, {'owners': owners})


def ownerAddView(request):
    if request.method == "POST":
        form = OwnerModelForm(request.POST)
        if form.is_valid():
            owner = form.save(commit=False)
            owner.name = request.POST.get('name')
            owner.phone = request.POST.get('phone')
            owner.address = request.POST.get('address')
            owner.save()
            return redirect('/owner/add')
                
    else:
        form = OwnerModelForm()
    return render(request, "./owner/add.html" , {'form': form})
