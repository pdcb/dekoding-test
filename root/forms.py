from django import forms
from .models import DogModel, OwnerModel

class DogModelForm(forms.ModelForm):

    class Meta:
        model = DogModel
        fields = ('name', 'breed', 'age', 'owner')


class OwnerModelForm(forms.ModelForm):

    class Meta:
        model = OwnerModel
        fields = ('name', 'phone', 'address')