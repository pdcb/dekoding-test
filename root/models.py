from django.db import models

class DogModel(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    breed = models.CharField(max_length=50)
    owner = models.CharField(max_length=50)

    def addDog(self,name,age,breed,owner):
        self.name = name
        self.age = age
        self.breed = breed
        self.owner = owner
        self.save()

    def __str__(self):
        return self.name


class OwnerModel(models.Model):
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=9)
    address = models.CharField(max_length=100)

    def addOwner(self,name,phone,address):
        self.name = name
        self.phone = phone
        self.address = address
        self.save()

    def __str__(self):
        return self.name
