# Dekoding Test Integration - Submit

Fork the application.

## Ubuntu Pre-requirements
We highly recommend to use a Linux Distribution like Ubuntu 18LTS updated.

Install Pip 3

    sudo apt-get install python3-pip

Install Virtualenvwrapper

    sudo pip3 install virtualenvwrapper

Add this at the end of ~/.bashrc 

    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
    source /usr/local/bin/virtualenvwrapper.sh

Execute ~/.bashrc

    source ~/.bashrc   

## Install Project

Setup Virtual Environment
    
    mkvirtualenv --python=/usr/bin/python3 dekoding_test

To access to the environment run
    
    workon dekoding_test
    
To exit
    
    deactivate

Clone Project
    
    git clone git@bitbucket.org:ework_diaz/dekoding-test.git dekoding_test
    cd dekoding_test

Install Requirements
    
    pip install -r requirements.txt

Run Migrations
    
    python3 manage.py migrate

Run Project
    
    python3 manage.py runserver
    
## Important
If a SyntaxError appears in the manage.py use the 'python3 manage.py ...' instead 'python manage.py ...'    
    